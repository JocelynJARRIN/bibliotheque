<?php
/**
 * Created by PhpStorm.
 * User: jocelyn.jarrin
 * Date: 30/09/2016
 * Time: 12:15
 */

namespace jocelynjarrin\bibliotheque\Helper;


class TimeHelper
{
    public function timeStrToTimeArray($transitionalHours, $transitionalMinutes, $transitionalSeconds)
    {
        if($transitionalSeconds > 59)
        {
            $seconds = $transitionalSeconds % 60;
            $transitionalMinutes = $transitionalMinutes + intval($transitionalSeconds / 60);
        }
        else
        {
            $seconds = $transitionalSeconds;
        }
        if($transitionalMinutes > 59)
        {
            $minutes = $transitionalMinutes % 60;
            $hours = $transitionalHours + intval($transitionalMinutes / 60);
        }
        else
        {
            $minutes = $transitionalMinutes;
            $hours = 0;
        }

        return array('hours' => $hours, 'minutes' => $minutes, 'seconds' => $seconds);
    }

    public function strToSeconds($str)
    {
        $t = explode(':', $str);
        $seconds = $t[0] * 3600 + $t[1] * 60 + $t[2];

        return $seconds;
    }

    public function secondsToStr($seconds)
    {
        $hours = intval($seconds / 3600) < 9 ? '0' . intval($seconds / 3600) : intval($seconds / 3600);
        $intermediaire = $seconds % 3600;
        $minutes = intval($intermediaire / 60) < 9 ? '0' . intval($intermediaire / 60) : intval($intermediaire / 60);
        $seconds = $seconds % 60 < 9 ? '0' . $seconds % 60 : $seconds % 60;

        return $hours . ':' . $minutes . ':' . $seconds;
    }


    public function tempsMoyen($dureeTotale, $nbAppel)
    {
        if($dureeTotale == NULL)
        {
            $tTempsMoyen = array('hours' => 0, 'minutes' => 0, 'seconds' => 0);
        }
        else
        {
            $tDureeTotale = explode(':', $dureeTotale);
            $transitionalHours = $tDureeTotale[0] * 3600;
            $transitionalMinutes = $tDureeTotale[1] * 60;
            $transitionalSeconds = $tDureeTotale[2];

            $totalSecondes = $transitionalHours + $transitionalMinutes + $transitionalSeconds;
            $hours = $minutes = $leftover = $seconds = 0;

            $moyenneSecondes = ($nbAppel == 0) ? 0 : intval($totalSecondes / $nbAppel);

            if($moyenneSecondes > 59)
            {
                $hours = intval($moyenneSecondes / 3600);
                $leftover = $moyenneSecondes % 3600;
            }
            else
            {
                $seconds = $moyenneSecondes;
            }
            if($leftover > 59)
            {
                $minutes = intval($leftover / 60);
                $seconds = $leftover % 60;
            }
            else
            {
                $minutes = $leftover;
                $hours = 0;
            }

            $tTempsMoyen = array('hours' => $hours, 'minutes' => $minutes, 'seconds' => $seconds);
        }

        return $tTempsMoyen;
    }

    public function arrayToHourFormat(array $array)
    {
        $str = NULL;

        if(!empty($array))
        {
            $array['hours'] = $array['hours'] < 10 ? '0' . $array['hours'] : $array['hours'];
            $array['minutes'] = $array['minutes'] < 10 ? '0' . $array['minutes'] : $array['minutes'];
            $array['seconds'] = $array['seconds'] < 10 ? '0' . $array['seconds'] : $array['seconds'];

            $str = $array['hours'] . ':' . $array['minutes'] . ':' . $array['seconds'];
        }

        return $str;
    }

    public function dureeSonnerie($requeteDureeSonnerie)
    {
        $tmp['sonnerie_inf_20'] = $tmp['sonnerie_20_30'] = $tmp['sonnerie_30_60'] = $tmp['sonnerie_sup_60'] = 0;

        try
        {
            asort($requeteDureeSonnerie);
        }catch(\Exception $ex)
        {
            return $tmp;
        }

        foreach($requeteDureeSonnerie as $item)
        {
            if($item < '00:00:20')
            {
                $tmp['sonnerie_inf_20']++;
            }
            elseif('00:00:20' > $item && $item < '00:00:30')
            {
                $tmp['sonnerie_20_30']++;
            }
            elseif('00:00:30' > $item && $item < '00:01:00')
            {
                $tmp['sonnerie_30_60']++;
            }
            elseif($item > '00:01:00')
            {
                $tmp['sonnerie_sup_60']++;
            }
        }

        return $tmp;
    }

    public function tableauCumule($tDurees)
    {
        if($tDurees != NULL && !empty($tDurees))
        {
            $transitionalSeconds = $transitionalMinutes = $minutes = $transitionalHours = $hours = $test = 0;

            foreach($tDurees as $duree)
            {
                $tDuree = explode(':', $duree);

                $transitionalSeconds = $transitionalSeconds + $tDuree[2];
                $transitionalMinutes = $transitionalMinutes + $tDuree[1];
                $transitionalHours = $transitionalHours + $tDuree[0];
            }

            return TimeHelper::timeStrToTimeArray($transitionalHours, $transitionalMinutes, $transitionalSeconds);
        }
        else
        {
            return array('hours' => 0, 'minutes' => 0, 'seconds' => 0);
        }
    }


}